# Reverse proxy

Allows for accessing multiple services on the host and providing HTTPS support.

See [nginx-proxy][0] for further details justifying the usage.

Pyinfra deploys the service on the host. 

## Usage

To allow new web services to be served by the reverse proxy, follow the configuration steps of [nginx-proxy][0].

[0]: https://github.com/nginx-proxy/nginx-proxy

## Debugging

* The Nginx config is automatically generated in `/etc/nginx/conf.d/default.conf`
