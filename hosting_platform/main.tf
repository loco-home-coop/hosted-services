locals {
  name = "loco-home-host"
  # https://docs.digitalocean.com/products/platform/availability-matrix/
  do_region = "lon1"
}