# Add DNS entries to CloudFlare. The reverse proxy on the host will take care of directing to the right container

# Was added manually by coop co-founder
data "cloudflare_zone" "locohome" {
  name = "locohome.coop"
}

# Point to Discourse
resource "cloudflare_record" "forum" {
  zone_id = data.cloudflare_zone.locohome.id
  name    = "forum.${data.cloudflare_zone.locohome.name}"
  value   = digitalocean_droplet.loco-home-host.ipv4_address
  type    = "A"
  # The DNS will show a CloudFlare IP instead of the droplet's IP
  proxied = true
}

# Point to Loco Home website
resource "cloudflare_record" "loco_website" {
  zone_id = data.cloudflare_zone.locohome.id
  name    = "preview.${data.cloudflare_zone.locohome.name}"
  value   = digitalocean_droplet.loco-home-host.ipv4_address
  type    = "A"
  # The DNS will show a CloudFlare IP instead of the droplet's IP
  proxied = true
}
