# Hosting platform deployment

**Dependencies:**
* [Terraform](https://www.terraform.io) scripts deploying a host in the cloud.
* [DigitalOcean](https://www.digitalocean.com/) is the chosen cloud provider.

## Deployment

Deployed by GitLab's CI/CD pipeline. Steps detailed in the root folder's README.

Terraform state is held in GitLab.
