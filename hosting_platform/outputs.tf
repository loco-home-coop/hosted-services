resource "local_file" "build_env" {
    content  = <<EOF
DROPLET_IP=${digitalocean_droplet.loco-home-host.ipv4_address}
EOF
    filename = "build.env"
}
