# To get your DigitalOcean SSH key IDs: `doctl compute ssh-key list`
variable "ssh_key_id" {
  description = "ID of the SSH key on DigitalOcean used in GitLab to push files"
}

variable "backup_service_ssh_key_id" {
  description = "Another SSH key ID on DigitalOcean. Device used to fetch droplet's backups"
}

variable "do_token" {
  description = "DigitalOcean access token"
}

variable "droplet_size" {
  description = "The size of the droplet"
  # https://registry.terraform.io/modules/terraform-digitalocean-modules/droplet/digitalocean/latest#droplet-sizes
  # nano: s-1vcpu-1gb
  # micro: s-2vcpu-2gb
  # small: s-2vcpu-4gb
  # Notes:
  #   - s-1vcpu-1gb: Discourse fails to precompile assets and the service stops
  #   - s-1vcpu-2gb: 10$/month Everything is super slow and CPU at 100% & memory close to 100%
  #   - s-2vcpu-2gb: 15$/month
  #   - s-2vcpu-4gb: 20$/month
  default = "s-2vcpu-2gb"
}

variable "cloudflare_api_token" {
  description = "CloudFlare API key with DNS zone edit permission"
}
