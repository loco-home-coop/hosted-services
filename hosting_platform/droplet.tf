# Droplet setup

data "template_file" "cloud-init-yaml" {
  template = file("${path.module}/cloud-init.yaml")
}

resource "digitalocean_droplet" "loco-home-host" {
  name      = local.name
  image     = "ubuntu-22-04-x64"
  region    = local.do_region
  size      = var.droplet_size
  ssh_keys  = [
    var.ssh_key_id,
    var.backup_service_ssh_key_id,
  ]
  # File stored in /var/lib/cloud/instances/<instance_id>/cloud-config.txt
  user_data = data.template_file.cloud-init-yaml.rendered
}

resource "digitalocean_firewall" "loco-home-host" {
  name = "only-allow-ssh-http-and-https"
  droplet_ids = [digitalocean_droplet.loco-home-host.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "80"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}