# Loco Home self-hosted services

Monorepo of [Loco Home](https://locohome.coop/) services hosted in the cloud. Prototypes and production.


## Services

Check the README file of each of the services for more details and for usage:
* [Discourse](discourse/README.md) communication platform
* [Hosting platform](hosting_platform/README.md)
* [Pyinfra](pyinfra_src/README.md) provisions the cloud machine hosting the services
* [Reverse proxy](reverse_proxy/README.md) to serve all web services running on the host with human-readable URLs and HTTPS


## Cloud platforms

* [DigitalOcean](https://www.digitalocean.com/) is hosting our services
* [CloudFlare](https://dash.cloudflare.com/) is the CDN directing DNS queries of our websites to our hosts
* GitLab is [managing the Terraform state](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html) of our services


## Usage

Generally, `make help` will print available commands.

### Run locally

**Dependencies:**
* [Docker](https://www.docker.com/)

Check `make help`.
* `make serve-website` and access the WordPress website on http://localhost:8080
* `make serve-forum` and access the Discourse forum on http://localhost:80
* Cleanup with `make clean-docker`

### Mock the server on a virtual machine

It is possible to run a virtual machine locally to exercise the provisioning and running of the services.
This is useful for debugging and to exercise disaster recovery.

Run `make help` to print the available rules.

**Dependencies:**
* Vagrant
* OpenSSL

**Steps:**
1. Add the following line to your `/etc/hosts` file. This lets your computer know that these website are hosted on the virtual machine
   ```
   192.168.57.10 preview.example.com forum.example.com # Loco Home on Vagrant
   ```
2. Start the virtual machine and deploy the resources. This may take a while as the VM gets provisioned in the background with [cloud-init.yaml](hosting_platform/cloud-init.yaml).
   ```bash
   cd pyinfra_src
   make start-vm
   make deploy-to-vm
   ```
3. The websites are available on the following addresses. Self-signed certificates are used to run the websites, most browsers will display a warning
   when navigating to these addresses for the first time. 
   * https://preview.example.com for the WordPress website
   * https://forum.example.com for the Discourse forum
4. To SSH on the machine: `make ssh-vm`
5. You can create a snapshot of your Vagrant box at any time for tinkering and being able to revert back to a known state:
   * Backup with `vagrant package --output my_backup.box; vagrant box add my_backup_name my_backup.box`
   * Do some hacking
   * Then restore with:
     * Stop the current VM `make destroy-vm`
     * Edit the existing Vagrantfile file to set `config.vm.box = my_backup_name`
     * `vagrant up`
   * Repeat!
6. Destroy all resources with `make clean` once done and remove the changes from `/etc/hosts`

Check the disaster recovery section for more advanced operations on the websites

### Deployment

Pushing commits to a branch will trigger a pipeline, that pipeline is defined in the [.gitlab-ci.yml](.gitlab-ci.yml) file.

Some steps of the pipline have to be triggered manually. Deployment is done in two steps:
1. Head to GitLab CI/CD page of the repository
2. Select the latest pipeline or the pipeline from your own branch
3. Click the button to manually start the deployment stage (Terraform)
4. After this stage is done, you can start the pyinfra stage manually too
   * In case this is the first time deployment, do not start the pyinfra stage too quickly as there are a few
     installation and setup steps that need time to process on the host.


## Updates

**Secrets:** Secrets files can be downloaded locally for inspections using a rule from the `Makefile`. These files can
then be updated locally and re-uploaded manually via GitLab's interface.

## Troubleshooting

### GitLab CI/CD cloud fails to authenticate to DigitalOcean

The error looks like:

> [...] GET https://api.digitalocean.com[...] Unable to authenticate you

1. Create a new Digital Ocean personal access token ([doc link](https://docs.digitalocean.com/reference/api/create-personal-access-token/)) with 1y expiry and full access
2. Update the GitLab secret CI/CD variable named `digital_ocean_token` ([doc link](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui))  


## Disaster Recovery

* [WordPress](loco_website/README.md)
* [Discourse](discourse/README.md)

## Contributions

Contributions are encouraged and welcome.
