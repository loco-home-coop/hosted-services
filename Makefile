include tools/makefiles/base_makefile.mk

WEBSITE_COMPOSE_CMD = cd loco_website; docker compose
FORUM_COMPOSE_CMD = cd discourse; docker compose

##@ Run

serve-website: ## Run the main website locally
	$(WEBSITE_COMPOSE_CMD) up
.PHONY: serve-website

serve-forum: ## Run the forum locally
	$(FORUM_COMPOSE_CMD) up
.PHONY: serve-forum


##@ GitLab

# Get secret files stored in GitLab that are used in CI/CD.
# Secrets stored just for the current session
# Code: https://docs.gitlab.com/ee/ci/secure_files/#use-secure-files-in-cicd-jobs
# Permission details: https://docs.gitlab.com/ee/ci/secure_files/index.html#security-details
.secure_files:
ifndef PRIVATE_TOKEN
	$(error Create a private token with read_api to be able to download secret files and set it as 'PRIVATE_TOKEN' environment variable)
endif
	tmp_dir=$$(mktemp --directory);\
	ln -s $${tmp_dir}/$@ $@;\
	cd $${tmp_dir};\
	export CI_PROJECT_ID=loco-home-coop/hosted-services;\
	curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/load-secure-files/-/raw/main/installer" | bash
	$(call echo_green_bold,Secret files successfully downloaded under '.secure_files/'. Warning this data is only available for the current session)

get-secret-files: .secure_files ## Download secret files from GitLab
.PHONY: get-secret-files


##@ Clean

clean:: clean-docker ## Clean docker and secure files
	# Remove secrets directory content and symbolic link
	rm -rf .secure_files/ .secure_files
.PHONY: clean

clean-docker: ## Stop Docker containers and remove their volumes
	$(call echo_bold,Stoping containers and removing their volumes)
	$(WEBSITE_COMPOSE_CMD) down --volumes
	$(FORUM_COMPOSE_CMD) down --volumes
.PHONY: clean-docker
