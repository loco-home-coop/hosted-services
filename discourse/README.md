# Discourse

[Discourse](https://www.discourse.org/) is the main communication platform chosen for the coop.

Inspired from [discourse-terraform](https://github.com/pearkes/discourse-terraform) repository.

* [Usage](#usage)
  * [Local deployment](#local-deployment)
  * [Manual steps for first installation](#manual-steps-for-first-installation)
* [CI/CD](#cicd)
* [Disaster recovery](#disaster-recovery)

## Usage

### Local deployment

It shouldn't be really useful to run Discourse locally since your local instance will not be representative of the Loco Home version.
We have not yet fully automated the setup of Discourse and a few manual steps are needed.

**Dependencies:**
* [Docker](https://www.docker.com/)

**Secrets (optional):**
This is required for having working e-mails.

You can download the environment file stored in GitLab's [_"Secure Files"_](https://gitlab.com/loco-home-coop/hosted-services/-/ci/secure_files?) locally to get the same setup.
The secure file name is likely to be `.discourse-compose.env`.
Follow [these instructions](https://docs.gitlab.com/ee/api/secure_files.html#download-secure-file) to download the file.
You will need to setup a GitLab access token for this.
Move that file locally as `hosted-services/discourse/.env`.

Example of script retrieving a secure file from GitLab (replace the access token with yours):
```bash
 #!/usr/bin/env bash
set -xeuo pipefail
secure_file_filename=".discourse-compose.env"
url_encoded_project_name="loco-home-coop%2Fhosted-services"
gitlab_api_url="https://gitlab.com/api/v4/projects/${url_encoded_project_name}"
private_access_token="<your_access_token>"

env_file_id=$(curl --header "PRIVATE-TOKEN: ${private_access_token}" "${gitlab_api_url}/secure_files" | jq ".[] | select(.name==\"${secure_file_filename}\").id")

curl --request GET --header "PRIVATE-TOKEN: ${private_access_token}" "${gitlab_api_url}/secure_files/${env_file_id}/download" --output .env

echo Done
```

**Run Discourse locally:**
```
docker compose up
# It can take a while for the service to be available
# Then open the following link in a browser: http://localhost
```

Login with username: `user` and password `bitnami123`. Head to `http://localhost/admin` and change the notification
e-mail address with the appropriate domain supported by the e-mail provider.

**Cleanup:**
```
docker compose down --volumes
```

### Manual steps for first installation

* On the Discourse website, logged in as an admin:
  * In _'Admin -> Settings -> Required'_: update the _'notification e-mail'_ domain to go from `noreply@forum.locohome.coop` to `noreply@locohome.coop`
  * Add custom color theme
  * Use the setup wizard on the Discourse webpage and update as follows:
    ```
    * Community name: Loco Home Forum
    * Describe your community in few words: Retrofitting community in Glasgow
    * Welcome topic:
      > Hi there and thanks for joining us! You can now start participating right now by creating new topics or responding to existing ones.
    
    # Access
    * Private
    * Contact:
       * Mail: (my e-mail)
       * Web Page: https://locohome.coop

    # Images
    * Use the images stored in `images/` to setup Discourse.

    # Organisation
      * Company Name: Loco Home
      * Governing Law: Scotland
      *  City for Disputes: Glasgow, UK
    ```

### CI/CD

Deployment of the service is mainly automated in the CI/CD pipeline.

The provisioning of the server is automated with [Pyinfra](../pyinfra_src/README.md).

The production settings are laid out in [`docker-compose.prod.yaml`](docker-compose.prod.yaml).
The file expects many environment variables to exist and contain secrets.
These are automatically read from a local `.env` file.
The one used for production containing all our secrets is stored in the repository CI/CD settings on GitLab as a "_Secure File_".
The file is copied on the host by Pyinfra during deployment.

The secure files can be downloaded locally using the `Makefile` at the root of the project.
Check the Pyinfra folder for more details on how to set up the GitLab settings.

## Updating Discourse online

* There is an automated Systemd service that updates Discourse monthly
* It is possible to update Discourse manually from a GitLab pipeline
  1. Trigger the manual Terraform deployment
  2. Then trigger the manual update stage

## Disaster recovery

### Backups

Automatic backups are done by a Systemd service triggered regularly by a timer.

### Restoring a backup

Steps to restore a backup on the host. Either for a 1st time installation or for reverting changes:

#### From a UI backup

To restore a backup obtained from the Discourse UI.

We should be able to upload the backup via the UI, but this is broken for now. Instead, we will copy the backup onto the discourse container filesystem 

1. Copy a backup file onto the host, or make it accessible somehow
2. Navigate to the Discourse installation folder (as described in the Pyinfra's `group_data` folder). Usually `/opt/locohome/discourse`
3. Copy the backup into the container. Make sure to copy the same filename name in the destination:
   ```bash
   # Create backup folder destination
   docker compose exec discourse mkdir -p /opt/bitnami/discourse/public/backups/default
   # Copy file into Discourse container
   docker compose cp /path/to/your/backup_name.tar.gz discourse:/opt/bitnami/discourse/public/backups/default/backup_name.tar.gz
   ```
4. Restore the backup from the UI of Discourse

#### From script

To restore a backup obtained from the `backup.sh` script

1. Login to the host either using an SSH connection or using the DigitalOcean interface.
2. Navigate to the Loco Home installation folder (as described in the Pyinfra's `group_data` folder). Usually `/opt/locohome`
3. Check that backups exist in the `backups/` folder
   1. If no backups are present, this means the host has been wiped from a previous deployment.
      You will have to upload backups stored elsewhere
   2. Contact an administrator that can point you to other backup storage locations
   3. Copy the backups in the `backups/` folder
4. Run the script `./backup.sh restore`
5. When prompted by the script, select the backup to restore
6. Restart Discourse `systemctl start discourse`
7. Restart the reverse proxy `systemctl start reverse_proxy`

## Notes

#### Useful files on Discourse container

* `/opt/bitnami/discourse/config/site_settings.yml`
* `/opt/bitnami/discourse/config/discourse.conf`
