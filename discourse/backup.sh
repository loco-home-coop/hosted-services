#!/usr/bin/env bash
# NAME
#   Discourse Loco Home service local backup
#
# SYNOPSIS
#   backup.sh <command>
#
# DESCRIPTION
#   Locally backs up or restore the Discourse service running as a docker compose service.
#   A local folder is mounted on the container to copy the files to backup/restore.
#   Files are compressed in an dated archive.
#   Previous backups are being cleaned in the process in order to keep:
#     - 35 daily backups
#   A systemd timer should schedule this backup script
#
# COMMANDS
#   backup
#     Backs up volumes from 'postgresql' and 'discourse' services into archives
#   cleanup
#     Remove backups older than 35days
#   restore
#     Restores most recent backup
: "${INSTALL_DIR:=/opt/locohome}"
. "${INSTALL_DIR}/tools/config.sh"

local_backup_folder="${LOCAL_BACKUPS_ROOT_FOLDER}/discourse"
compose_exec=(docker compose --file docker-compose.yaml --file docker-compose.prod.yaml)

function cleanup {
  echoBold "### Removing backups older than 35 days"
  find "${local_backup_folder}" -type f -mtime +35 -delete
}

function backup() {
  mkdir -p "${local_backup_folder}"
  tmp_backup_dir=$(mktemp -d)
  today=$(date +"%Y-%m-%d_%H-%M-%S")

  echoBold "### Copying service files from volume locally"
  "${compose_exec[@]}" run --rm -v "${tmp_backup_dir}":/backups discourse \
    cp -a /bitnami/discourse /backups
  "${compose_exec[@]}" run --rm --user "$(id --user)" -v "${tmp_backup_dir}":/backups postgresql \
    cp -a /bitnami/postgresql /backups

  echoBold "### Compressing into backup file"
  tar -cpf "${tmp_backup_dir}/discourse.tar.gz" --directory "${tmp_backup_dir}/discourse" -I "gzip --best" .
  tar -cpf "${tmp_backup_dir}/postgresql.tar.gz" --directory "${tmp_backup_dir}/postgresql" -I "gzip --best" .
  tar -cf "${local_backup_folder}/discourse_${today}.tar" --directory "${tmp_backup_dir}" discourse.tar.gz postgresql.tar.gz

  echoBold "### Cleaning up"
  rm -rf ${tmp_backup_dir}
  cleanup
}

function restore() {
  echoBold "### Selecting the backup to restore"
  backup_filename=$(choose_file_from_folder "${local_backup_folder}")

  echoBold "### Stopping services"
  systemctl stop discourse

  tmp_backup_dir=$(mktemp -d)
  echoBold "### Extracting '${backup_filename}' in temporary restoration dir"
  tar xf "${backup_filename}" --directory "${tmp_backup_dir}"

  echoBold "### Restoring backup '${backup_filename}'"
  "${compose_exec[@]}" run --rm --volume "${tmp_backup_dir}:/backups" discourse bash -c \
    'rm -rf /bitnami/discourse/*; tar -xf "/backups/discourse.tar.gz" -C /bitnami/discourse'
  "${compose_exec[@]}" run --rm --user "$(id --user)" --volume "${tmp_backup_dir}:/backups" postgresql bash -c \
    'rm -rf /bitnami/postgresql/*; tar -xf "/backups/postgresql.tar.gz" -C /bitnami/postgresql'

  echoBold "### Cleaning up"
  rm -rf "${tmp_backup_dir}"
}

while [[ "$#" -gt 0 ]]; do
    case $1 in
    backup) backup ;;
    cleanup) cleanup ;;
    restore) restore ;;
    *)
      echoError "Unknown parameter passed: $1"
      usage
      exit 1
      ;;
    esac
    shift
done

echoGreen "### Done"
