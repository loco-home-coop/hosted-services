# Loco Home Discourse images

The images in this folder are to be used either with Discourse's setup wizard or in https://forum.locohome.coop/admin/site_settings/category/branding

* `32-loco_square.png`: Favicon. Make sure it is different than the one of https://locohome.coop
* `120-loco_square.png`: _'Logo small'_, used as the system's avatar in the forum. The logo is shifted up to prevent the round avatar from cutting the logo
* `1417-loco_square.png`: High resolution square logo
* `loco_linear_RGB.png`: Banner. Copied from https://locohome.coop/
