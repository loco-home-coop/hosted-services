import os

environment = "prod"

ssh_key = os.environ["DROPLET_PRIVATE_SSH_KEY_PATH"]
ssh_user = "root"
