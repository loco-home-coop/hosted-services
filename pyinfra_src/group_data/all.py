from dotenv import load_dotenv
# Note: 'from pathlib import Path' fails Pyinfra's `debug-inventory` command which will try parsing `Path` into a str.
#   This is a bug in Pyinfra.
import pathlib

load_dotenv()

base_dir = pathlib.Path(__file__).resolve().parent.parent.parent
dest_apps_dir = pathlib.Path("/opt/locohome")

## Environment
loco_group_name = "loco-home"

## Backups
dest_backups_dir = dest_apps_dir / "backups"

## Discourse
source_discourse_dir = base_dir / "discourse"
dest_discourse_dir = dest_apps_dir / "discourse"
dest_discourse_backup_dir = dest_backups_dir / "discourse"

## Loco website
source_loco_website_dir = base_dir / "loco_website"
dest_loco_website_dir = dest_apps_dir / "loco_website"
dest_loco_website_backup_dir = dest_backups_dir / "loco_website"

## Reverse proxy
source_reverse_proxy_dir = base_dir / "reverse_proxy"
dest_reverse_proxy_dir = dest_apps_dir / "reverse_proxy"

## Systemd
dest_systemd_dir = pathlib.Path("/etc/systemd/system")

## Tools
source_tools_dir = base_dir / "tools"
dest_tools_dir = dest_apps_dir / "tools"
