from pyinfra import local

local.include("tasks/environment.py")
local.include("tasks/virtual_machine.py")
local.include("tasks/deploy_reverse_proxy.py")
local.include("tasks/deploy_loco_website.py")
local.include("tasks/deploy_discourse.py")
