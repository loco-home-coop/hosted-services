# Pyinfra

[Pyinfra](https://pyinfra.com/) is a configuration management tool. It is used to manage the services host.

The tool is idempotent, meaning running the scripts can be run multiple time and always lead to the same state.

## CI/CD

Pyinfra needs to be able to send commands to the host using SSH.
The encryption key is stored in the repository CI/CD settings on GitLab as a _"Secure File"_.
The secure files can be downloaded locally using the `Makefile` at the root of this project.
The key is downloaded when a job is running.
