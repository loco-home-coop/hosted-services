from pyinfra import host
from pyinfra.operations import files

from tasks import _helpers


def upload_files():
    files.directory(
        path=host.data.dest_tools_dir,
    )
    _helpers.upload_data_file(
        host.data.source_tools_dir / "config.sh",
        host.data.dest_tools_dir,
    )


upload_files()
