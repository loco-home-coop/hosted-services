from pyinfra.operations import systemd

systemd.service(
    service="discourse-update",
    running=True,
    daemon_reload=True,
)
