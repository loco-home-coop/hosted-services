from pyinfra import host

from tasks import _helpers


def upload_files():
    _helpers.create_directory(host.data.dest_discourse_dir, sudo=True)
    _helpers.create_directory(host.data.dest_discourse_backup_dir)

    _helpers.upload_data_file(
        host.data.source_discourse_dir / "docker-compose.yaml",
        host.data.dest_discourse_dir,
    )
    _helpers.upload_data_file(
        host.data.source_discourse_dir / f"docker-compose.{host.data.dict()['environment']}.yaml",
        host.data.dest_discourse_dir,
    )
    _helpers.upload_data_file(
        host.data.source_discourse_dir / "backup.sh",
        host.data.dest_discourse_dir,
        mode="744",
    )

    if host.data.dict()["environment"] == "prod":
        _helpers.upload_data_file(
            host.data.source_discourse_dir / ".env",
            host.data.dest_discourse_dir,
            mode="600",
        )


def upload_services():
    _helpers.upload_systemd_service(
        host.data.source_discourse_dir / "discourse.service.j2",
        enabled=True,
        running=True,
    )
    _helpers.upload_systemd_service(
        host.data.source_discourse_dir / "discourse-update.service.j2",
        enabled=False,
        running=False,
    )
    _helpers.upload_systemd_service(
        host.data.source_discourse_dir / "discourse-update.timer",
        enabled=True,
        running=True,
    )
    _helpers.upload_systemd_service(
        host.data.source_discourse_dir / "discourse-backup.service",
        enabled=False,
        running=False,
    )
    _helpers.upload_systemd_service(
        host.data.source_discourse_dir / "discourse-backup.timer",
        enabled=True,
        running=True,
    )


upload_files()
upload_services()
