from pyinfra import host
from pyinfra.facts.server import Which
from pyinfra.operations import server


def update_vagrant_user():
    fish_path = host.get_fact(Which, "fish")
    server.user(
        user='vagrant',
        groups=[
            host.data.loco_group_name,
            # "sudo",
            "docker",
            "systemd-journal",  # Users in groups 'adm', 'systemd-journal' can see all journal messages
            "vagrant",
        ],
        shell=fish_path,
        _sudo=True,
    )


update_vagrant_user()
