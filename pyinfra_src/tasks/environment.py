from pyinfra import host
from pyinfra.operations import server

from tasks import _helpers


def create_groups():
    server.group(
        name=f"Create '{host.data.loco_group_name}' group",
        group=host.data.loco_group_name,
        _sudo=True,
    )


def create_default_folders():
    _helpers.create_directory(directory=host.data.dest_apps_dir, sudo=True)


create_groups()
create_default_folders()
