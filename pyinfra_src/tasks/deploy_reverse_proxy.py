from pyinfra import host
from pyinfra.operations import files

from tasks import _helpers


def upload_files():
    files.directory(
        path=host.data.dest_reverse_proxy_dir.as_posix(),
    )
    _helpers.upload_data_file(
        host.data.source_reverse_proxy_dir / "docker-compose.yaml",
        host.data.dest_reverse_proxy_dir,
    )
    _helpers.upload_data_file(
        host.data.source_reverse_proxy_dir / "discourse_settings.conf",
        host.data.dest_reverse_proxy_dir,
    )
    _helpers.upload_data_file(
        host.data.source_reverse_proxy_dir / "loco_website_settings.conf",
        host.data.dest_reverse_proxy_dir,
    )
    if (host.data.source_reverse_proxy_dir / f"docker-compose.{host.data.dict()['environment']}.yaml").exists():
        _helpers.upload_data_file(
            host.data.source_reverse_proxy_dir / f"docker-compose.{host.data.dict()['environment']}.yaml",
            host.data.dest_reverse_proxy_dir,
        )


def upload_services():
    _helpers.upload_systemd_service(
        host.data.source_reverse_proxy_dir / "reverse_proxy.service.j2",
        enabled=True,
        running=True,
    )


upload_files()
upload_services()
