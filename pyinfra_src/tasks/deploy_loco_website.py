from pyinfra import host

from tasks import _helpers


def upload_files():
    _helpers.create_directory(host.data.dest_loco_website_dir)
    _helpers.create_directory(host.data.dest_loco_website_backup_dir)

    _helpers.upload_data_file(
        host.data.source_loco_website_dir / "Dockerfile",
        host.data.dest_loco_website_dir,
    )
    _helpers.upload_data_file(
        host.data.source_loco_website_dir / "docker-compose.yaml",
        host.data.dest_loco_website_dir,
    )
    _helpers.upload_data_file(
        host.data.source_loco_website_dir / f"docker-compose.{host.data.dict()['environment']}.yaml",
        host.data.dest_loco_website_dir,
    )
    _helpers.upload_data_file(
        host.data.source_loco_website_dir / "backup.sh",
        host.data.dest_loco_website_dir,
        mode="744",
    )
    _helpers.upload_data_file(
        host.data.source_loco_website_dir / "upload_file_size.ini",
        host.data.dest_loco_website_dir,
    )

    if host.data.dict()["environment"] == "prod":
        _helpers.upload_data_file(
            host.data.source_loco_website_dir / ".env",
            host.data.dest_loco_website_dir,
            mode="600",
        )


def upload_services():
    _helpers.upload_systemd_service(
        host.data.source_loco_website_dir / "loco_website.service.j2",
        enabled=True,
        running=True,
    )
    _helpers.upload_systemd_service(
        host.data.source_loco_website_dir / "loco_website-backup.service",
        enabled=False,
        running=False,
    )
    _helpers.upload_systemd_service(
        host.data.source_loco_website_dir / "loco_website-backup.timer",
        enabled=True,
        running=True,
    )


upload_files()
upload_services()
