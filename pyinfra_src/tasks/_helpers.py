import types
from pathlib import Path

from pyinfra import host
from pyinfra.operations import files, systemd

# Grab all our Pyinfra variables and remove the odd elements
template_variables = {
    k:v for k,v in host.data.dict().items() if not isinstance(v, types.ModuleType)
}


def upload_systemd_service(src: Path, enabled=True, running=True):
    filename = src.name.removesuffix(".j2")
    files.template(
        src=src.as_posix(),
        dest=(host.data.dest_systemd_dir / filename).as_posix(),
        mode="644",
        user="root",
        group="root",
        _sudo=True,
        **template_variables,
    )
    systemd.service(
        service=filename,
        enabled=enabled,
        running=running,
        daemon_reload=True,
        _sudo=True,
    )


def upload_data_file(src: Path, dest_folder_name: Path, new_filename: str = None, mode: str = "660"):
    filename = new_filename if new_filename else src.name.removesuffix(".j2")
    files.template(
        src=src.as_posix(),
        dest=(dest_folder_name / filename).as_posix(),
        mode=mode,
        **template_variables,
    )


def create_directory(directory: Path, sudo: bool = False):
    """
    Create a directory with custom permissions.
    - The owner can read and write
    - Users belonging to the default group of the application can read and write. "set-group-id" bit is set, allowing
      created files and folders to inherit the parent directory permissions.
    - Anyone else is denied access.
    :param directory: The directory to create
    :param sudo: Use sudo permissions to create the directory
    """
    files.directory(
        path=directory.as_posix(),
        mode="u+rw,g+srw,o-rwx",
        group=host.data.loco_group_name,
        _sudo=sudo,
    )
