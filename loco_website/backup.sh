#!/usr/bin/env bash
# NAME
#   Loco Home website service local backup
#
# SYNOPSIS
#   backup.sh <command>
#
# DESCRIPTION
#   Locally backs up or restore the Loco Home website service running as a docker compose service.
#   The full containers volumes content are backed up and stored in archives.
#   Previous backups are being cleaned in the process.
#
# COMMANDS
#   backup
#     Create a backup stored in the backup folder of the host
#   cleanup
#     Remove backups older than 35days
#   restore
#     Restores most recent backup

: "${INSTALL_DIR:=/opt/locohome}"
. "${INSTALL_DIR}/tools/config.sh"

local_backup_folder="${LOCAL_BACKUPS_ROOT_FOLDER}/loco_website"
compose_exec=(docker compose --file docker-compose.yaml --file docker-compose.prod.yaml)

function cleanup {
  echoBold "### Removing backups older than 35 days"
  find "${local_backup_folder}" -type f -mtime +35 -delete
}

function backup() {
  mkdir -p "${local_backup_folder}"
  tmpBackupDir=$(mktemp -d)
  today=$(date +"%Y-%m-%d_%H-%M-%S")
  backup_filename="loco_website_${today}.tar.gz"

  echoBold "### Copying service files into backup archive"
  "${compose_exec[@]}" cp --archive wordpress:/var/www/html - > "${tmpBackupDir}/wordpress.tar"
  "${compose_exec[@]}" cp --archive db:/var/lib/mysql - > "${tmpBackupDir}/db.tar"

  tar -cpf "${local_backup_folder}/${backup_filename}" -C "${tmpBackupDir}" -I "gzip --best" .

  echoBold "### Cleaning up"
  rm -rf "${tmpBackupDir}"
  cleanup
}

function restore() {
  tmpBackupDir=$(mktemp -d)

  echoBold "### Selecting the backup to restore"
  backup_filename=$(get_latest_folder_file "${local_backup_folder}")
  echoBold "### File selected: '${backup_filename}'"

  echoBold "### Restoring backup '${local_backup_folder}/${backup_filename}'"
  tar -xvf "${local_backup_folder}/${backup_filename}" -C "${tmpBackupDir}"
  # Clearing up volumes to avoid just overwriting existing files with backup and leave potential other files
  "${compose_exec[@]}" rm --force --stop --volumes wordpress db
  # Creating container to create volumes
  "${compose_exec[@]}" create db wordpress
  "${compose_exec[@]}" cp --archive - wordpress:/var/www < "${tmpBackupDir}/wordpress.tar"
  "${compose_exec[@]}" cp --archive - db:/var/lib < "${tmpBackupDir}/db.tar"

  echoBold "### Cleaning up"
  rm -rf ${tmpBackupDir}
}

while [[ "$#" -gt 0 ]]; do
    case $1 in
    backup) backup ;;
    cleanup) cleanup ;;
    restore) restore ;;
    *)
      echoError "Unknown parameter passed: $1"
      usage
      exit 1
      ;;
    esac
    shift
done

echoGreen "### Done"
