# WordPress

Main website of Loco Home: https://locohome.coop

Running with the official WordPress Docker container. Backed up automatically.

* [Usage](#usage)
  * [Local deployment](#local-deployment)
  * [Manual steps for first installation](#manual-steps-for-first-installation)
* [CI/CD](#cicd)
* [Disaster recovery](#disaster-recovery)

## Deployment

### Automatic steps

Deployment of the service is mainly automated in the CI/CD pipeline.

The provisioning of the server is automated with [Pyinfra](../pyinfra_src/README.md).

The production settings are laid out in [`docker-compose.prod.yaml`](docker-compose.prod.yaml).
The file expects many environment variables to exist and contain secrets.
These are automatically read from a local `.env` file.
The one used for production containing all our secrets is stored in the repository CI/CD settings on GitLab as a "_Secure File_".
The file is copied on the host by Pyinfra during deployment.

Check the Pyinfra folder for more details on how to set up the GitLab settings.

### Manual steps

Some steps are to be done  manually if starting the website from scratch

### DiscourseWP plugin

Install the [DiscourseWP](https://en-gb.wordpress.org/plugins/wp-discourse/) plugin and follow the setup where WordPress is the DiscourseConnect provider
and the Discourse forum being the client. Our users list is managed by the MemberPress plugin.

To limit access of the forum only to users having an active membership, add the following piece of code in your `functions.php` file.
This will be the file of your active theme, for example `/var/www/html/wp-content/themes/blocksy-child/functions.php`

```php
// Custom code added by Loco Home. Checking a user has an active MemberPress membership before
// allowing them to connect to the forum.
// Resources:
// * https://meta.discourse.org/t/manage-group-membership-in-discourse-with-wp-discourse-sso/74724
// * https://stackoverflow.com/a/66383603
add_action( 'wpdc_sso_provider_before_sso_redirect', 'wpdc_custom_check_user_membership', 10, 2 );
function wpdc_custom_check_user_membership( $user_id, $user ) {
        // Check if the user has an active subscription
        $member = new MeprUser($user_id);
        if (!$member->is_active) {
                // Redirect to subscription page if no active subscription
                wp_safe_redirect(home_url('register/regular-member'));
                exit;
        }
}
```

## Disaster recovery

### Backups

Automatic backups are done by a Systemd service triggered regularly by a timer.

You can have access to the backup script locally to test the backup and restore mechanism with: 

```
export INSTALL_DIR=..
# Choose any backup location of your choice
export LOCAL_BACKUPS_ROOT_FOLDER=$(pwd)/backups
mkdir ${LOCAL_BACKUPS_ROOT_FOLDER}
./backup.sh backup
```

### Restoring a backup

**Prerequisites:**
* Assuming you have a fresh installation of WordPress
* You must be an administrator of the website
* You have a local copy os a backup downloaded from the 'All-in-One WP Migration' plugin

**Steps:**
1. Fill up the welcome page, it does not have to be valid, only remember the username and password you are setting.
   All these will be wiped when the backup is restored.
2. Login with the new credentials
3. Install and activate the `All-in-One WP Migration` plugin
4. Navigate to the plugin "Import" tab and import the local backup
5. Let the plugin guide the restore process but stop at the _'Your site has been imported successfully!'_ dialog.
   We need to fix the Permalink structure by forcing WordPress to do some refresh:
   * In that dialog, click on the _'Save permalinks structure. (opens a new window)'_ link in the dialog
   * Login with your real admin credentials
   * Without changing anything, click "Save Changes"


## Issues

* In the 'widget' admin page we get the following error message:
  > Notice: Function wp_enqueue_script() was called incorrectly. "wp-editor" script should not be enqueued together with the new widgets editor (wp-edit-widgets or wp-customize-widgets). Please see Debugging in WordPress for more information. (This message was added in version 5.8.0.) in /var/www/html/wp-includes/functions.php on line 5835 
  * If either of the following plugin is activated, the error message is displayed:
    * `WP-Discourse`
    * `MemberPress Basic`
  * No clue how to fix this. The Discourse plugin GitHub repo doesn't have any issues referring to this.
