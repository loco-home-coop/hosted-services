#!/usr/bin/env bash
# SYNOPSIS
#   Common bash scripting configurations
# USAGE
#   Source the file at the top of your script
#     . <PATH_TO_FILE>/config.sh
set -xeuo pipefail

#
# Constants
#

: "${LOCAL_BACKUPS_ROOT_FOLDER:=/opt/locohome/backups}"

if [ ! -d "${LOCAL_BACKUPS_ROOT_FOLDER}" ]; then
  echo "Backup folder '${LOCAL_BACKUPS_ROOT_FOLDER}' does not exist. Is it mounted?"
  exit 1
fi

#
# Helpers
#

function echoBold() {
  printf "$(tput bold)$1$(tput sgr0)\n"
}

function echoError() {
  printf "$(tput setaf 1)$1$(tput sgr0)\n"
}

function echoWarning() {
  printf "$(tput setaf 3)$1$(tput sgr0)\n"
}

function echoGreen() {
  printf "$(tput setaf 2)$1$(tput sgr0)\n"
}

function get_latest_folder_file() {
  # From a folder, prints the most recent file according to its modified time
  #   Arg1: Absolute path prefix to look for. e.g. /mnt/folder/prefix
  # Always append slash by deleting the shortest match of string in $1 from the end
  searchPrefix="${1%/}/"
  latestFile=""

  for file in "$searchPrefix"*; do
    [[ "$file" -nt "$latestFile" ]] && latestFile=$file
  done

  latestFile=$(basename "$latestFile")

  if [ -z "$latestFile" ]; then
    echo "### Error when looking up for latest backup. Provided glob is:"
    echo "### $searchPrefix*"
    echo "### 'ls' content is"
    ls "$searchPrefix"*
    exit 1
  fi

  echo $latestFile
}

function choose_file_from_folder() {
  # Let the user choose a file from a folder
  #   Arg1: Absolute path to look for. e.g. /mnt/folder/prefix/
  PS3="Select a file to use: "
  select file in "$1"/*; do
    break
  done
  echo $file
}

function is_container_running() {
  # Expected argument:
  #   Arg1: container_name
  #         Full name of the container
  container_name=$1
  if [ -z "$(docker container ls -q --filter name="$container_name")" ]; then
    return 1
  else
    return 0
  fi
}