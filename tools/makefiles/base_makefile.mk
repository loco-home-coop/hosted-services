# DESCRIPTION
# 	Generic helpers for using GNU Make
#
# USAGE
#	Include the file first in your Makefile.
#	Run `make help` to see a list of targets having provided some help.
#

define USAGE
Usage:
	- Call a specific rule
		make <rule_name>
	- Calls a makerule with specific arguments
		make <rule_name> FOO=bar

Generic arguments:
	DEBUG                          Depending on the environment, will be set to "y" or unset. Current: $(DEBUG)
endef

#
# Environment
#
DEBUG ?= n

#
# Public helpers
#
# Current commit SHA value
COMMIT_HASH = $(shell git rev-parse HEAD | cut -c1-7)
BUILD_TAG = $(shell git rev-parse HEAD | cut -c1-7)
BRANCH_NAME ?= $(shell git branch --show-current)

# 'CI' is set in GitLab pipelines https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
ifeq ($(CI),true)
  IS_CI ?= y
else
  IS_CI ?= n
endif

define echo_bold
  echo $$(tput bold)### $(1)$$(tput sgr0)
endef
define echo_green_bold
  echo $$(tput bold;tput setaf 2)### $(1)$$(tput sgr0)
endef
define echo_orange_bold
  echo $$(tput bold;tput setaf 3)### $(1)$$(tput sgr0)
endef
define echo_red_bold
  echo $$(tput bold;tput setaf 1)### $(1)$$(tput sgr0)
endef
# Determines if a variable is defined or not
# PARAMETERS
#	1. Name of the variable to check
# USAGE
#	$(if $(call defined,VARIABLE),echo defined,echo undefined)
defined = $(strip $(filter-out undefined,$(flavor $1)))

# Displays a bold prompt waiting for user input. Any answer other than ["Y", "y", "yes"] will exit the program
# 	- argument 1: The text to display
# USAGE
#	@$(call yes_no_prompt,Turn off Mind Control Facility? [y/n]: )
define yes_no_prompt =
  @read -e -p "$$(tput bold)$(1)$$(tput sgr0)" confirmation;\
  if [[ $$confirmation != "Y" && $$confirmation != "y" && $$confirmation != "yes"  ]]; then \
    echo "Exited by user";\
    exit 1;\
  fi
endef

# Log command output in file. Further commands will have their output be recorded in a file as well as being printed on screen.
# This means color will not be displayed as they programs will know they are in a non-interactive shell.
# use `unbuffer program [ args ]` to have colors back (but also the logs having the color codes)
# Usage:
# 		$(call log_output,stdout.log);\ # Watch out for spaces after the comma
#		unbuffer npm install
# - arg1: Log filename
define log_output
  exec 1> >(tee "$(1)") 2>&1
endef

# To be used as newline when calling $(error) or $(info) in make
# https://stackoverflow.com/a/17055840
define n


endef

#
# Private helpers & parameters
#

SHELL := bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
.SHELLFLAGS := -euo pipefail -c
# Referring to Make variables that don't exist will warn you
MAKEFLAGS += --warn-undefined-variables

TOOLS_DIRECTORY := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))/../tools

#
# Rules
#
.DEFAULT_GOAL := help

print-%: ## Print % variable
	@echo '$(subst ','\'',$*=$($*))'

# Borrowed from https://www.thapaliya.com/en/writings/well-documented-makefiles/
# Targets ending with a double comment sign will be displayed in the help along with the comment.
help: ## Display this help section
	$(eval export USAGE)
	@echo "$$USAGE"
	@echo -e "\nMake rules available:\n"
	@echo $$(tput bold)Imported targets$$(tput sgr0)
	@awk 'BEGIN {FS = ":.*##"; printf "\033[36m\033[0m"} /^[-.$$\(\)%/a-zA-Z_0-9]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $$(echo $(MAKEFILE_LIST) | tr ' ' '\n' | tac | tr '\n' ' ')
.PHONY: help
