# DESCRIPTION
#	Allows using pyenv-virtualenv when not loaded in the current environment.
#
# USAGE
#	- These makefiles must be included before any other:
#		- ./base_makefile.mk
#	- Look at USAGE_PYTHON or execute 'make help'

#
# Private helpers & parameters
#
define USAGE_PYTHON
$n
Python arguments:
	PYTHON_EXEC                    Python executable to use. Current: '$(PYTHON_EXEC)'
endef
USAGE += $(USAGE_PYTHON)

define activate-venv
	source $(PYTHON_VENV_NAME)/bin/activate
endef

PYTHON_EXEC ?= python
PYTHON_VENV_NAME = venv

#
# Rules
#
$(PYTHON_VENV_NAME):
	@$(call echo_bold,"Creating python virtual environment '$@'")
	$(PYTHON_EXEC) -m venv $@

create-venv: $(PYTHON_VENV_NAME) ## Create Python virtual environment
.PHONY: create-venv

requirements%txt.dep: requirements%txt $(PYTHON_VENV_NAME)
	@$(call echo_bold,"Installing python requirements from $<")
	$(activate-venv);\
	pip install --upgrade -r $<
	touch $@

install-all-python-requirements: requirements.txt.dep ## Install all requirements
.PHONY: install-all-python-requirements

clean::
	@$(call echo_bold,"Deleting python venv '$(PYTHON_VENV_NAME)'")
	rm -rf $(PYTHON_VENV_NAME)
	find . -name 'requirements*.txt.dep' -delete
.PHONY: clean
